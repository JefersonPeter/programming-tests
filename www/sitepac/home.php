<?php include 'head.php'; ?><!-- INCLUSÃO DE ARQUIVO PHP -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css?version=12" type="text/css">
<div class="backall">

<!-- SESSÃO DA PÁGINA PRINCIPAL -->
<section class="my-5">
  <div class="container">
    <div class="row">
      <div class="col-md-10 mx-auto">
		<h1><center><strong>Bem vindo a JP ELETRONICS</strong></center></h1><br>
		<div class="text-main">
        <h5 align="justify" ><strong>Aqui você encontra as melhores opções de produtos eletrônicos,em relação a periféricos como teclado mouses e HD's, com acesso também a SSD's.</strong></h5>
        <h5 align="justify" ><strong>Se você é aquela pessoa que gosta de jogar, você encontra tudo aqui, inclusive as placas de videos mais poderosas da atualidade.</strong></h5>
		<h5 align="justify" ><strong>Se é uma empresa, e procura não apenas desempenho, mas também armazenamento e não quer se preocupar com Backups e perdas de arquivos, durante 
		uma queda de energia, a sua solução é o NAS(armazenamento conectado à rede),com capacidade de backups automaticos e suporte quando há uma queda de energia
		,também possui armazenamentos de até 1000 TB.</strong></p>
	</div>
  </div>
  </div>
</section>

<?php include 'footer.php'; ?><!-- INCLUSÃO DE ARQUIVO PHP -->
</div>
# Atualizações sobre o projeto:

### Para fazer este projeto utilizei as seguintes ferramentas:

1. XAMPP(contém mysql e php integrado)
2. Templates como Bootstrap
3. PHPmyAdmin para rodar o projeto

### Nos arquivos temos:
1. A pasta www/sitepac/... e seus respectivos arquivos;
2. Na pasta img estão as imagens;
3. Na pasta Css os arquivos de estilização;
4. Na pasta js os arquivos js. 

### Arquivos principais em PHP:
* Basicamente nos arquivos que contém toda a informação do site possuímos um 
```<?php include 'nome_do_arquivo.php'; ?>```
que serve basicamente para deixar mais dinânico e rápido de carregar a página, por isso que footer.php e head.php tem pouco código e nos outros arquivos os chamo para completar o site.

**OBS:** comecei a desenvolver o projeto com php e mysql, mais não tenho muito experiência os dois juntos, então fiz uma demonstração de como o site ficaria sem a parte do backend, usando PHP que deixa o site um pouco mais dinâmico.
Estou vendo um pouco de mysql agora no 3º semestre.


**Para acessar (rodar) o arquivo na web deve-se instalar o XAMPP e ativar o apache, após feito isso deve-se digitar ```Localhost/www/sitepac/cadastro.php```... para cair na página de cadastro. e clicar-se em registrar-se, login e assim sucessivamente**

**OBS01:** Para acessar o arquivo na web, além de digitar na barra do link, deve-se pegar a pasta www, e coloca-lá dentro de htdocs na pasta do XAMPP e para executar o painel de controle do XAMPP, procure por **XAMPP CONTROL** que dará acesso ao painel, e após isso apenas ativar o apache e digitar o endereço na barra de link, por padrão não há necessidade de digitar HTDOCS após LocalHost.

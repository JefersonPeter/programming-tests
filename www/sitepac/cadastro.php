<head><meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>JP ELETRONICS</title>
<link rel="stylesheet" href="css/cadastro.css" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.js"></script></head>
<!-- PÁGINA DO CADASTRO -->
<body>
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-xl-9 mx-auto">
        <div class="card card-signin flex-row my-5">
          <div class="card-img-left d-none d-md-flex">
             <!-- Background image for card set in CSS! -->
          </div>
          <div class="card-body">
            <h5 class="card-title text-center">Cadastro</h5>
            <form class="form-signin">
              
			  <div class="form-label-group">
                <input type="text" id="inputUserame" class="form-control" placeholder="Nome completo" required autofocus>
                <label for="inputUserame">NOME</label>
              </div>
			  
			  <div class="form-label-group">
                <input type="text" id="inputUserame" class="form-control" placeholder="Telefone" required autofocus>
                <label for="inputUserame">Telefone</label>
              </div>
			  
			  <div class="form-label-group">
                <input type="text" id="inputUserame" class="form-control" placeholder="Celular" required autofocus>
                <label for="inputUserame">Celular</label>
              </div>
			  
			  <div class="form-label-group">
                <input type="text" id="inputUserame" class="form-control" placeholder="Endereço" required autofocus>
                <label for="inputUserame">Endereço</label>
              </div>
			  
			  <div class="form-label-group">
                <input type="text" id="inputUserame" class="form-control" placeholder="Bairro" required autofocus>
                <label for="inputUserame">Bairro</label>
              </div>
			  
			  <div class="form-label-group">
                <input type="text" id="inputUserame" class="form-control" placeholder="Complemento" required autofocus>
                <label for="inputUserame">Complemento</label>
              </div>
			  
			  <div class="form-label-group">
                <input type="text" id="inputUserame" class="form-control" placeholder="Cidade" required autofocus>
                <label for="inputUserame">Cidade</label>
              </div>
			  
			  <div class="form-label-group">
                <input type="text" id="inputUserame" class="form-control" placeholder="CEP" required autofocus>
                <label for="inputUserame">CEP</label>
              </div>
			  
			  <div class="form-label-group">
                <input type="date" id="inputUserame" class="form-control" placeholder="Cadastro" required autofocus>
                <label for="inputUserame">Data de cadastro</label>
              </div>
			  
			  <div class="form-label-group">
                <input type="text" id="inputUserame" class="form-control" placeholder="E-mail" required autofocus>
                <label for="inputUserame">E-mail</label>
              </div>
              
              <hr>
			  
              <div class="form-label-group">
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                <label for="inputPassword">Password</label>
    	</div>

		<a type="submit" href="login.php" class="btn btn-lg btn-primary btn-block text-uppercase"><strong>Registrar-se</strong></a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
<?php include 'footer.php'; ?><!-- INCLUSÃO DE ARQUIVO PHP -->
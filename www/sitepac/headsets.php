<?php include 'head.php'; ?><!-- INCLUSÃO DE ARQUIVO PHP -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link rel="stylesheet" href="css/item.css?version=12" type="text/css">
<div class="backall">
			<!-- SESSÃO DOS ITENS DA LOJA CRIADA -->
			<section>
                <div class="col-md-10 mx-auto">
					<div class="about-container">
					<p><strong><center>HEADSET'S</center></strong></p>
						<div class="about-card">
							<p><strong><center>Headset AlienWare</center></strong></p>
							<img class="img-fluid" src="/www/sitepac/img/headsetalienware.png">
							<p>Headset AlienWare gaming, alta performance, sem fio.</p>
							<hr>
							<p><Strong>R$800,00</strong> Á VISTA.</p>
							<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
						</div>
						<div class="about-card middle-card">
							<p><strong><center>Headset Gaming Azul</center></strong></p>
							<img class="img-fluid" src="/www/sitepac/img/headsetblue.png"
							<p>Headset gaming, alta performance, LED azul forte.</p>
							<hr>
							<p><Strong>R$249,50</strong> Á VISTA.</p>
							<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
						</div>
						<div class="about-card">
							<p><strong><center>Headset Gaming Vermelho</center></strong></p>
							<img class="img-fluid" src="/www/sitepac/img/headsetver.png">
							<p>Headset vermelho gaming, alta performance.</p>
							<hr>
							<p><Strong>R$352,78</strong> Á VISTA.</p>
							<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
						</div>
				
					
						<div class="about-container">
						<div class="about-card">
							<p><strong><center>Headset Logitech</center></strong></p>
							<img class="img-fluid" src="/www/sitepac/img/headset logitech.png" >
							<p>Headset Logitech, alta performance.</p>
							<hr>
							<p><Strong>R$150,33</strong> Á VISTA.</p>
							<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
						</div>
						<div class="about-card middle-card">
						<p><strong><center>Headset OEX gaming</center></strong></p>
							<img class="img-fluid" src="/www/sitepac/img/headsetoex.png">
							<p>Headset OEX gaming, alta performance</p>
							<hr>
							<p><Strong>R$120,99</strong> Á VISTA</p>
							<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
						</div>
						<div class="about-card">
						<p><strong><center>Headset Razer Gaming</center></strong></p>
							<img class="img-fluid" src="/www/sitepac/img/headsetrazer.png">
							<p>Headset Razer gaming, alta performance.</p>
							<hr>
							<p><Strong>R$800,00</strong> Á VISTA</p>
							<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
						</div>
					</div>
				</div>
				</section> <!--  TERMINO DA SESSÃO DOS ITENS DA LOJA CRIADA, ABAIXO SEGUE O MESMO ESQUEMA -->
				
					<section>
						<div class="col-md-10 mx-auto">		
						<div class="about-container">
							<p><strong><center>PLACAS DE VÍDEO</center></strong></p>
							<div class="about-card">
							<p><strong><center>Placa de Vídeo NVIDIA GEFORCE RTX - 2060 </center></strong></p>
								<img class="img-fluid"  src="/www/sitepac/img/rtx2060.png" >
								<p> 12 nm, base no processador gráfico TU106.</p>
								<hr>
								<p><Strong>R$2.000,00</strong> Á VISTA.</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
							<div class="about-card middle-card">
							<p><strong><center>Placa de Vídeo NVIDIA GEFORCE GTX - 1660TI </center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/gtx1660ti.png">
								<p>GPU opera na frequência de 1500 MHz.</p>
								<hr>
								<p><Strong>R$1.685,76</strong> Á VISTA.</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
							<div class="about-card">
							<p><strong><center>Placa de Vídeo NVIDIA GEFORCE GTX - 1080 </center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/gtx1080.png">
								<p>16 nm, baseado no processador gráfico GP104.</p>
								<hr>
								<p><Strong>R$1.300,78</strong> Á VISTA.</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
						</div>
						
							<div class="about-container">
							<div class="about-card">
							<p><strong><center>Placa de Vídeo NVIDIA GEFORCE RTX - 2080TI </center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/rtx2080ti.png">
								<p>GPU tecnologias inovadoras e 11 GB de memória GDDR6.</p>
								<hr>
								<p><Strong>R$7.800,70</strong> Á VISTA.</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
							<div class="about-card middle-card">
								<p><strong><center>Placa de Vídeo NVIDIA GEFORCE RTX - 2080 </center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/rtx2080.png">
								<p>GPU tecnologias inovadoras e 11 GB de memória GDDR6. </p>
								<hr>
								<p><Strong>R$4.200,60</strong> Á VISTA</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
							<div class="about-card">
								<p><strong><center>Placa de Vídeo NVIDIA GEFORCE RTX - 2070 </center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/rtx2070.png">
								<p>12 nm, base no processador gráfico TU106,variante TU106-400A-A1.</p>
								<hr>
								<p><Strong>R$2.500,00</strong> Á VISTA</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
						</div>
					</div>	
					</section>
					
						<section>
						<div class="col-md-10 mx-auto">		
						<div class="about-container">
							<p><strong><center>MOUSES</center></strong></p>
							<div class="about-card">
								<p><strong><center>Mouse C3TECH </center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/c3techgamer.png" >
								<p> Mouse ótimo para trabalhos, iluminação LED.</p>
								<hr>
								<p><Strong>R$40,00</strong> Á VISTA.</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
							<div class="about-card middle-card">
								<p><strong><center>Mouse Logitech </center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/mouseLogitech.png">
								<p>Mouse Gamer Logitech, bom para jogos leves.</p>
								<hr>
								<p><Strong>R$65,79</strong> Á VISTA.</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
							<div class="about-card">
							<p><strong><center>Mouse Microsoft </center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/mousemicrosoft.png" >
								<p>Mouse ótimo para empresas.</p>
								<hr>
								<p><Strong>R$60,00</strong> Á VISTA.</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
						</div>
						
							<div class="about-container">
							<div class="about-card">
								<p><strong><center>Mouse Multilaser </center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/mousesemfio.png" >
								<p> Ótima opção para quem não gosta de fios, sem fio de alta qualidade.</p>
								<hr>
								<p><Strong>R$58,70</strong> Á VISTA.</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
							<div class="about-card middle-card">
								<p><strong><center>Mouse Gamer RedDragon</center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/mousereddragon.png" >
								<p>Para quem gosta de um pouco mais de precisão e resposta.</p>
								<hr>
								<p><Strong>R$125,12</strong> Á VISTA</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
							<div class="about-card">
								<p><strong><center>Mouse Wolf </center></strong></p>
								<img class="img-fluid" src="/www/sitepac/img/mousewolf.png">
								<p>Se você gosta de qualidade e precisão, este é o mouse perfeito para você.</p>
								<hr>
								<p><Strong>R$170,87</strong> Á VISTA</p>
								<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
							</div>
						</div>
					</div>	
					</section>
					
							<section>
							<div class="col-md-10 mx-auto">		
							<div class="about-container">
								<p><strong><center>MOUSEPAD'S</center></strong></p>
								<div class="about-card">
									<p><strong><center>Mousepad Dagger </center></strong></p>
									<img class="img-fluid" src="/www/sitepac/img/mousepad.png" >
									<p> Mousepad estiloso e chamativo</p>
									<hr>
									<p><Strong>R$37,00</strong> Á VISTA.</p>
									<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
								</div>
								<div class="about-card middle-card">
									<p><strong><center>Mousepad Preto </center></strong></p>
									<img class="img-fluid" src="/www/sitepac/img/mousepad4.png" >
									<p>Mousepad com escosto para a mão.</p>
									<hr>
									<p><Strong>R$42,45</strong> Á VISTA.</p>
									<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
								</div>
								<div class="about-card">
									<p><strong><center>Mousepad Listrado </center></strong></p>
									<img class="img-fluid" src="/www/sitepac/img/mousepad5.png" >
									<p>Mousepad com detalhes minimalistas.</p>
									<hr>
									<p><Strong>R$46,00</strong> Á VISTA.</p>
									<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
								</div>
							</div>
							
								<div class="about-container">
								<div class="about-card">
									<p><strong><center>Mousepad Tecnológico </center></strong></p>
									<img class="img-fluid" src="/www/sitepac/img/mousepad6.png" >
									<p>Mousepad com detalhe tecnológico.</p>
									<hr>
									<p><Strong>R$49,99</strong> Á VISTA.</p>
									<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
								</div>
								<div class="about-card middle-card">
									<p><strong><center>Mousepad Predator </center></strong></p>
									<img class="img-fluid" src="/www/sitepac/img/mousepadpredator.png" >
									<p>Mousepad Predator com detalhe em LED, USB.</p>
									<hr>
									<p><Strong>R$88,15</strong> Á VISTA</p>
									<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
								</div>
								<div class="about-card">
									<p><strong><center>Mousepad RedDragon </center></strong></p>
									<img class="img-fluid" src="/www/sitepac/img/mousepadred.png" >
									<p>Mousepad RedDragon com detalhe em vermelho.</p>
									<hr>
									<p><Strong>R$110,33</strong> Á VISTA</p>
									<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
								</div>
							</div>
						</div>	
						</section>
						
							<section>
									<div class="col-md-10 mx-auto">	
									<div class="about-container">
										<p><strong><center>HD'S</center></strong></p>
										<div class="about-card">
										<p><strong><center>HD externno SanDisk 120 GB </center></strong></p>
											<img class="img-fluid" src="/www/sitepac/img/hd6.png" >
											<p>HD externo pequeno de 120 GB.</p>
											<hr>
											<p><Strong>R$140,00</strong> Á VISTA.</p>
											<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
										</div>
										<div class="about-card middle-card">
										<p><strong><center>HD externo Samsung 500 GB </center></strong></p>
											<img class="img-fluid"  src="/www/sitepac/img/hdsamsung.png" >
											<p>HD externo  para quem precisa de mais espaço.</p>
											<hr>
											<p><Strong>R$320,85</strong> Á VISTA.</p>
											<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
										</div>
										<div class="about-card">
										<p><strong><center>HD externo Seagate 1 TB </center></strong></p>
											<img class="img-fluid" src="/www/sitepac/img/hdsea1tb.png" >
											<p>HD externo essencial para filmes.</p>
											<hr>
											<p><Strong>R$590,99</strong> Á VISTA.</p>
											<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
										</div>
									</div>
									
										<div class="about-container">
										<div class="about-card">
										<p><strong><center>HD externo Seagate 2TB </center></strong></p>
											<img class="img-fluid" src="/www/sitepac/img/hdsea2tb.png" >
											<p>HD externo essencial para qualquer dado.</p>
											<hr>
											<p><Strong>R$750,23</strong> Á VISTA.</p>
											<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
										</div>
										<div class="about-card middle-card">
										<p><strong><center>HD exteno Samsung 4 TB </center></strong></p>
											<img class="img-fluid" src="/www/sitepac/img/hddam.png" >
											<p>HD externo essencial para você.</p>
											<hr>
											<p><Strong>R$1.000,00</strong> Á VISTA</p>
											<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
										</div>
										<div class="about-card">
										<p><strong><center>HD externo Seagate 8 TB </center></strong></p>
											<img class="img-fluid" src="/www/sitepac/img/hdsea8tb.png">
											<p>Espaço de sobra para seus arquivos.</p>
											<hr>
											<p><Strong>R$1.250,33</strong> Á VISTA</p>
											<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
										</div>
									</div>
								</div>	
								</section>
								
									<section>
										<div class="col-md-10 mx-auto">		
										<div class="about-container">
											<p><strong><center>SSD'S</center></strong></p>
											<div class="about-card">
											<p><strong><center>SSD Seagate 500 GB </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/sddfire.png" >
												<p>SSD tipo NVME.</p>
												<hr>
												<p><Strong>R$420,45</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card middle-card">
											<p><strong><center>SSD Samsung 512 GB </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/sddsam4tb.png" >
												<p>SSD tipo NVME/M.2.</p>
												<hr>
												<p><Strong>R$432,15</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card">
											<p><strong><center>SDD Samsung 500 GB </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/sddsan500.png">
												<p>SSD tipo NVME/M.2.</p>
												<hr>
												<p><Strong>R$402,54</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
										</div>
										
											<div class="about-container">
											<div class="about-card">
											<p><strong><center>SSD Seagate 4 TB </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/ssdsea4tb.png" >
												<p>SSD tipo SATA III.</p>
												<hr>
												<p><Strong>R$1.750,25</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card middle-card">
											<p><strong><center>SSD Samsung 500 GB </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/ssdnvmesang.png" >
												<p>SSD tipo NVME/M.2.</p>
												<hr>
												<p><Strong>R$654,60</strong> Á VISTA</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card">
											<p><strong><center>SSD Seagate 500 GB </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/sddsea.png" >
												<p>SSD tipo SATA III.</p>
												<hr>
												<p><Strong>R$782,75</strong> Á VISTA</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
										</div>
									</div>	
									</section>
									
										<section>
										<div class="col-md-10 mx-auto">		
										<div class="about-container">
											<p><strong><center>TECLADOS</center></strong></p>
											<div class="about-card">
											<p><strong><center>Teclado HP </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/tecladohp.png" >
												<p>Teclado comfortável, exclusividade HP.</p>
												<hr>
												<p><Strong>R$42,75</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card middle-card">
											<p><strong><center>Teclado Gamer Ryzer </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/ryzer.png">
												<p>Teclado Gamer Ryzer Retroiluminado.</p>
												<hr>
												<p><Strong>R$67,28</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card">
											<p><strong><center>Teclado Gamer RedDragon </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/redragon.png">
												<p>Teclado gamer mecânico e retroiluminado.</p>
												<hr>
												<p><Strong>R$140,69</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
										</div>
											<div class="about-container">
											<div class="about-card">
											<p><strong><center>Teclado Gamer Predator </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/predator.png" >
												<p>Com funções exclusivas.</p>
												<hr>
												<p><Strong>R$531,25</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card middle-card">
											<p><strong><center>Teclado Gamer AlienWare </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/alienware.png" >
												<p>Teclado mecânico em aço escovado.</p>
												<hr>
												<p><Strong>R$727,89</strong> Á VISTA</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card">
											<p><strong><center>Teclado Gamer Warrior </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/warrior.png" >
												<p>Teclado retroiluminado.</p> 
												<hr>
												<p><Strong>R$768,12</strong> Á VISTA</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
										</div>
									</div>
									</section>
									
										<section>
										<div class="col-md-10 mx-auto">	
										<div class="about-container">
											<p><strong><center>ARMAZENAMENTO EM NUVEM</center></strong></p>
											<div class="about-card">
											<p><strong><center>NAS com 10 baias </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/nas516.png">
												<p>Armazenar em Nuvem de até 64 TB.</p>
												<hr>
												<p><Strong>R$5.620,23</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card middle-card">
											<p><strong><center>NAS com 12 baias </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/nas64696.png">
												<p>Armazenar em Nuvem de até 100 TB.</p>
												<hr>
												<p><Strong>R$6.782.33</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card">
											<p><strong><center>NAS com 8 baias </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/nas64.png">
												<p>Armazenar em Nuvem de até 48 TB.</p>
												<hr>
												<p><Strong>R$5.230,25</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
										</div>
										
											<div class="about-container">
											<div class="about-card">
												<p><strong><center>NAS com 4 baias </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/nas4b.png" >
												<p>Armazenar em Nuvem de até 16 TB.</p>
												<hr>
												<p><Strong>R$3.520,89</strong> Á VISTA.</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card middle-card">
												<p><strong><center>NAS com 5 baias </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/nas2.png" >
												<p>Armazenar em Nuvem de até 32 TB.</p>
												<hr>
												<p><Strong>R$4.278,52</strong> Á VISTA</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
											<div class="about-card">
												<p><strong><center>NAS com 16 baias </center></strong></p>
												<img class="img-fluid" src="/www/sitepac/img/NAS1.png"  >
												<p>Armazenar em Nuvem de até 1 PB.</p>
												<hr>
												<p><Strong>R$50.256,21</strong> Á VISTA</p>
												<a type="submit" href="finalizar.php" class="btn btn-outline-success"><strong>COMPRAR</strong></a>
											</div>
										</div>
									</div>	
									</section>
<?php include'footer.php'?>
									</div>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>JP ELETRONICS</title>
<link rel="stylesheet" href="css/style.css?version=12" type="text/css">
<link rel="stylesheet" href="css/item.css?version=12" type="text/css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.js"></script> 
<!-- CABEÇALHO DO SITE -->
<header>
  <div class="overlay"></div>
  <div class="container h-100">
    <div class="d-flex h-100 text-center align-items-center">
      <div class="w-100 text-white">
        <h1 class="display-3"><strong>JP ELETRONICS</strong></h1><br>
          <p class="lead mb-0"><a href="Home.php"  class="btn btn-outline-success"><strong>HOME</strong></a>&emsp;&emsp;<a href="headsets.php"  class="btn btn-outline-success"><strong>Produtos</strong></a>&emsp;&emsp;
		<a href="about.php"  class="btn btn-outline-success" ><strong>Sobre</strong></a></p>
      </div>
    </div>
  </div>
</header>

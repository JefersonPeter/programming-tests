<?php include 'head.php'; ?><!-- INCLUSÃO DE ARQUIVO PHP -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link rel="stylesheet" href="css/final.css?version=12" type="text/css">
<div class="backall">
<br>
<!-- SESSÃO PARA FINALIZAÇÃO DE COMPRA -->
 <section> 
		<div class="col-md-10 mx-auto">
            <div class="services-container">
                <div class="service-box">
                    <div class="service-img">
                        <img src="/www/sitepac/img/headsetalienware.png" class="img-thumbnail" >
                    </div>
                    <div class="service-description">
                        <h3 id="gerenciamento-title">Heaset alienware</h3>
                        <hr>
							<p><Strong>R$800,00</strong> Á VISTA.</p>
					</div>
                </div>			
			</div>
		</div>
		<div class="col-md-10 mx-auto">
            <div class="services-container">
                <div class="service-box">
                    <div class="service-img">
                       <img src="/www/sitepac/img/rtx2080ti.png" class="img-thumbnail">
                    </div>
                    <div class="service-description">
                        <h3 id="gerenciamento-title">Placa de Vídeo NVIDIA GEFORCE RTX - 2080TI</h3>
						<hr>
						<p><Strong>R$7.800,70</strong> Á VISTA.</p>
					</div>
                </div>			
			</div>
		</div>
		<div class="col-md-10 mx-auto">
            <div class="services-container">
                <div class="service-box">
                    <div class="service-img">
                       <img src="/www/sitepac/img/ssdsea4tb.png" class="img-thumbnail">
                    </div>
                    <div class="service-description">
                        <h3 id="gerenciamento-title">SSD Seagate 4 TB</h3>
						<hr>
						<p><Strong>R$1.750,25</strong> Á VISTA.</p>
					</div>
                </div>			
			</div>
		</div>
		<br>
		<div class="col-md-10 mx-auto">
            <div class="services-container">
                <div class="service-box">
                    <div class="service-img">
                       <img src="/www/sitepac/img/comprafinal.png" width="200px" height="200px">
                    </div>
                    <div class="service-description">
                        <h2 id="gerenciamento-title">TOTAL</h3>
						<hr>
						<p><Strong>FRETE: R$15,00</strong></p>
						<p><Strong>R$10.365,95</strong> Á VISTA.</p>
						<center><a type="submit" href="pagfinal.php" class="btn btn-outline-danger"><strong>COMPRAR</strong></a></center>
					</div>
                </div>			
			</div>
		</div>
</section>
<hr>
<?php include'footer.php'?>
